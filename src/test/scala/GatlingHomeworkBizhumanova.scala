import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class GatlingHomeworkBizhumanova extends Simulation {

  private val httpProtocol = http
    .baseUrl("http://wiremock.atlantis.t-systems.ru")
    .inferHtmlResources(AllowList(), DenyList())
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7")
    .upgradeInsecureRequestsHeader("1")
    .userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36")

  private val headers_0 = Map(
    "Cache-Control" -> "no-cache",
    "Pragma" -> "no-cache"
  )


  private val scn = scenario("GatlingHomeworkBizhumanova")
    .exec(
      http("request_0")
        .get("/tau/albizhum")
        .headers(headers_0)
        .check(status.is(200))
    )
  //check percentage of failed requests
  setUp(scn.inject(atOnceUsers(100))).
    assertions(forAll.failedRequests.percent.around(2, 2), global.responseTime.max.lte(700)).
    protocols(httpProtocol)
}

