enablePlugins(GatlingPlugin)

name := "Gatling_homework"

version := "0.1"

scalaVersion := "2.13.7"



libraryDependencies += "io.gatling" % "gatling-core" % "3.7.2"

libraryDependencies += "io.gatling" % "gatling-test-framework" % "3.7.2"

libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts" % "3.7.2"

